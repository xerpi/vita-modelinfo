TARGET		= modelinfo
TARGET_OBJS	= main.o
LIBS		= -lSceIofilemgrForDriver_stub -lSceSysrootForKernel_stub -lSceSysclibForDriver_stub

PREFIX  = arm-vita-eabi
CC      = $(PREFIX)-gcc
AS      = $(PREFIX)-as
OBJCOPY = $(PREFIX)-objcopy
CFLAGS  = -Wl,-q -Wall -O0 -mcpu=cortex-a9
LDFLAGS = $(CFLAGS)
ASFLAGS =

all: $(TARGET).skprx

%.skprx: %.velf
	vita-make-fself -c $< $@

%.velf: %.elf
	vita-elf-create -e $(TARGET).yml $< $@

$(TARGET).elf: $(TARGET_OBJS)
	$(CC) $(LDFLAGS) -nostartfiles $^ $(LIBS) -o $@

clean:
	@rm -rf $(TARGET).skprx $(TARGET).velf $(TARGET).elf $(TARGET_OBJS)

send: $(TARGET).skprx
	curl -T $(TARGET).skprx ftp://$(PSVITAIP):1337/ux0:/data/tai/kplugin.skprx
	@echo "Sent."
