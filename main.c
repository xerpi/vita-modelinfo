#include <string.h>
#include <psp2kern/kernel/modulemgr.h>
#include <psp2kern/kernel/sysmem.h>
#include <psp2kern/io/fcntl.h>

typedef struct SceSysrootBuffer {
	unsigned short Version;
	unsigned short size;
	unsigned int Current_Firmware_Version;
	unsigned int Firmware_Version_Shipped_from_Factory;
	unsigned int unk0C[(0x2C - 0x0C) / 4];
	unsigned long long Bitfield_flags;
	unsigned int unk34[(0x40 - 0x34) / 4];
	unsigned int Devkit_Function_address_1;
	unsigned int Devkit_UID;
	unsigned int Devkit_Function_address_2;
	unsigned int ASLR_Seed;
	unsigned int Devkit_Config_Flags;
	unsigned int Devkit_Config_Flags2;
	unsigned int Devkit_Config;
	unsigned int Devkit_Config_Flags3;
	unsigned int DRAM_base_paddr;
	unsigned int DRAM_size;
	unsigned int unk68;
	unsigned int Boot_type_indicator_1;
	unsigned char OpenPsId[0x10];
	unsigned int secure_kernel_enp_raw_data_paddr;
	unsigned int secure_kernel_enp_size;
	unsigned int unk88;
	unsigned int unk8C;
	unsigned int kprx_auth_sm_self_raw_data_paddr;
	unsigned int kprx_auth_sm_self_size;
	unsigned int prog_rvk_srvk_raw_data_paddr;
	unsigned int prog_rvk_srvk_size;
	unsigned short Model;
	unsigned short Device_type;
	unsigned short Device_config;
	unsigned short Type;
	unsigned short unkA8[(0xB0 - 0xA8) / 2];
	unsigned char Session_ID[0x10];
	unsigned int unkC0;
	unsigned int Boot_type_indicator_2;
	unsigned int unkC8[(0xD0 - 0xC8) / 4];
	unsigned int suspend_saved_context_paddr;
	unsigned int unkD4[(0xF8 - 0xD4) / 4];
	unsigned int BootLoader_Revision;
	unsigned int Sysroot_Magic_value;
	unsigned char Encrypted_Session_Key[0x20];
} __attribute__((packed)) SceSysrootBuffer;

#define strwrite(fd, str, ...) \
	do { \
		char __buff[128]; \
		snprintf(__buff, sizeof(__buff), str, __VA_ARGS__); \
		ksceIoWrite(fd, __buff, strlen(__buff)); \
	} while (0)

void _start() __attribute__ ((weak, alias("module_start")));

int module_start(SceSize argc, const void *args)
{
	SceSysrootBuffer *sysroot_buffer;
	SceUID fd;

	fd = ksceIoOpen("ux0:modelinfo.txt", SCE_O_WRONLY | SCE_O_CREAT | SCE_O_TRUNC, 6);
	if (fd < 0)
		return SCE_KERNEL_START_FAILED;

	sysroot_buffer = ksceKernelGetSysrootBuffer();

	strwrite(fd, "Model: 0x%04X\n", sysroot_buffer->Model);
	strwrite(fd, "Device_type: 0x%04X\n", sysroot_buffer->Device_type);
	strwrite(fd, "Device_config: 0x%04X\n", sysroot_buffer->Device_config);
	strwrite(fd, "Type: 0x%04X\n", sysroot_buffer->Type);
	strwrite(fd, "unkD4: 0x%08X\n", sysroot_buffer->unkD4[0]);

	ksceIoClose(fd);

	return SCE_KERNEL_START_SUCCESS;
}

int module_stop(SceSize argc, const void *args)
{
	return SCE_KERNEL_STOP_SUCCESS;
}
